//import 'bootstrap/dist/css/bootstrap.min.css';

const { Pool } = require('pg');
const express = require('express');
const app = express();
const session = require('express-session');
const path = require('path');

const cors = require('cors');

app.use(cors());

//json and Forms posting.
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

//send folder
app.use('/assets', express.static(path.join(__dirname, '/public')));

// Add session as middleware
app.use(session({
    secret: 'secret',
    resave: true,
    cookie: { maxAge: 8 * 60 * 60 * 1000 },
    saveUninitialized: true
}));

//
const pool = new Pool({
    database: 'd9s3fj5r0l5cem',
    host: 'ec2-54-83-9-36.compute-1.amazonaws.com',
    user: 'ydsbualamojhmp',
    password: '8d1f8fbef494ae7f63e5dc6ca2493727df08dfb3afbfcee5e0dc5640201a2686',
    port: 5432
});

/*-----------------
 * VIEWS
 ----------------*/

app.get('/', (req, resp) => {
    resp.sendFile(path.join(__dirname, 'views', 'login.html'));
});

app.get('/home', (req, resp) => {

    if(req.session.loggedin) {
        resp.sendFile(path.join(__dirname, 'views', 'index.html'));
    } else {
        resp.redirect('/');

    }
    
});

app.get('/contact', (req, resp) => {
    resp.sendFile(path.join(__dirname, 'views', 'contact.html'));
});

app.get('/about', (req, resp) => {
    resp.sendFile(path.join(__dirname, 'views', 'about.html'));
});

 /*-----------------
 * SERVER
 ----------------*/

 app.post('/auth', async (req, resp) => {
    const { username, password } = req.body;
    console.log(username, password);

    if(!username || !password) {
        resp.status(401).json({
            message: 'Please enter username and password'
        });
    }

    try {
        const result = await pool.query('SELECT username, password FROM login_info WHERE username = $1 AND password = $2', [username, password]);
        

        if (result.rowCount > 0) {

            req.session.loggedin = true;
            req.session.username = username;

            resp.redirect('/home');
        } else {
            resp.json({
                message: 'User is not registered'
            });
        }
    } catch(e) {
        resp.status(500).json({
            success: false,
            error: e.message
        })
    }
 });


app.listen(process.env.PORT || 5000);

console.log("Hello Node");